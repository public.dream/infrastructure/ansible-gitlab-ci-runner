# Ansible Gitlab Runner

An `ansible-pull` repo to configure a Gitlab Runner with optional executors.

## Executors

- Docker
This is the standard Docker executor.

- Custom KVM
This is a custom qemu/kvm executor using shell scripts.
